const dianoche = document.getElementById("dia-noche");
dianoche.addEventListener("change", function() {
    if(this.checked) {
        document.body.classList.add("dark-mode");
        document.querySelector(".header").classList.add("dark-mode");
        document.querySelector("nav.menus").classList.add("dark-mode");
        document.querySelectorAll("nav.menus ul li a").forEach(function(el) {
            el.classList.add("dark-mode");
        });
        document.querySelector(".nombre").classList.add("dark-mode");
        document.querySelector(".main h2").classList.add("dark-mode");
        document.querySelector('.footer').classList.add("dark-mode");
        document.querySelector('.intro').classList.add("dark-mode"); // Agrega dark-mode a .intro
        document.querySelector('.header').classList.add("dark-mode")
    } else {
        document.body.classList.remove("dark-mode");
        document.querySelector(".header").classList.remove("dark-mode");
        document.querySelector("nav.menus").classList.remove("dark-mode");
        document.querySelectorAll("nav.menus ul li a").forEach(function(el) {
            el.classList.remove("dark-mode");
        });
        document.querySelector(".nombre").classList.remove("dark-mode");
        document.querySelector(".main h2").classList.remove("dark-mode");
        document.querySelector('.footer').classList.remove("dark-mode");
        document.querySelector('.intro').classList.remove("dark-mode"); // Remueve dark-mode de .intro
        document.querySelector('.header').classList.remove("dark-mode")
    }
});